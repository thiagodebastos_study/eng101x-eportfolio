import React, { Component } from "react";
import Link from "gatsby-link";
import { Div } from "glamorous";

export default class SiteHeader extends Component {
  state = {
    isOpen: false
  };

  toggleNav = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <header>
        <Div
          onClick={this.toggleNav}
          css={{
            ":hover": {
              cursor: "pointer"
            }
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            height="32"
            viewBox="0 0 32 32"
            width="32"
          >
            <path d="M4 10h24a2 2 0 0 0 0-4H4a2 2 0 0 0 0 4zm24 4H4a2 2 0 0 0 0 4h24a2 2 0 0 0 0-4zm0 8H4a2 2 0 0 0 0 4h24a2 2 0 0 0 0-4z" />
          </svg>
        </Div>

        {this.state.isOpen && (
          <nav>
            <div>
              {/* loop through site pages */}
              {this.props.navItems.map((item, index) => (
                <Link to={`/${item}`} key={index}>
                  <li>{item}</li>
                </Link>
              ))}
            </div>
          </nav>
        )}
      </header>
    );
  }
}
