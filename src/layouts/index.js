import React from "react";
import { Div, H3 } from "glamorous";
import { css } from "glamor";
import Link from "gatsby-link";

import SiteHeader from "../components/SiteHeader";
import { rhythm } from "../utils/typography";

const linkStyle = css({ float: `right` });

export default ({ children, data }) => (
  <Div
    margin={`0 auto`}
    maxWidth={700}
    padding={rhythm(2)}
    paddingTop={rhythm(1.5)}
  >
    <SiteHeader
      navItems={["about-me", "wp1", "wp2", "wp3", "writers-journals"]}
    />
    <Link to={`/`}>
      <H3 marginBottom={rhythm(2)} display={`inline-block`}>
        {data.site.siteMetadata.title}
      </H3>
    </Link>
    {children()}
  </Div>
);

export const query = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`;
