import React from "react";

export default () => (
  <div>
    <h3>Writer's Journals</h3>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis, quam magni
      dolores, nemo nihil iste quae totam sint vel magnam tenetur laudantium
      voluptas doloremque ducimus voluptates asperiores exercitationem minus
      corrupti! Saepe laboriosam unde corrupti atque sint, porro cumque delectus
      blanditiis minus quia omnis laudantium. Minus, temporibus ad! Optio culpa
      quo ducimus odio, animi sed cumque eveniet ipsum. Ipsum, quibusdam veniam.
      Provident maiores animi vel recusandae sint ad atque, tenetur ipsam
      perferendis earum, culpa magni libero dignissimos? Amet quos, doloribus ad
      atque necessitatibus dolorum autem, aspernatur velit maiores laudantium
      sit a. Eaque temporibus quis asperiores blanditiis quo ex pariatur nisi
      illo ut recusandae, neque, perferendis itaque cupiditate accusantium ea
      perspiciatis sint dolorem. Est, suscipit laudantium a sint esse omnis
      recusandae expedita. Numquam quod ratione neque ex qui temporibus
      excepturi saepe obcaecati quia, culpa nostrum eaque, natus nam iure
      perspiciatis perferendis, nemo vel voluptatum architecto totam quo
      officiis in? Unde, impedit tempore! Temporibus quibusdam nulla iusto
      exercitationem necessitatibus sed maxime consequatur recusandae iure?
    </p>
    <p>
      Atque, commodi possimus? Praesentium ipsum ut rerum eum iure impedit
      veniam culpa nobis. Minima praesentium accusantium neque? Perferendis,
      soluta! Vero ratione sed sequi amet obcaecati placeat iste! Ipsa dolores
      tempore architecto facilis earum nulla quidem cum distinctio illo nemo
      voluptatibus veniam aspernatur, non enim recusandae totam? Nemo, dolore
      ad? Quasi aliquam beatae, nostrum atque repellendus maxime quod commodi
      quisquam eligendi dolores nisi quidem dolor soluta enim iste magnam sed,
      blanditiis tempore quae delectus recusandae dolorum veniam deleniti. Modi,
      optio? Doloribus quasi consectetur officiis numquam voluptates molestias.
      Aliquid odio facilis, iure qui ab excepturi deleniti earum dolorem quasi
      optio exercitationem eligendi quidem corporis consequuntur explicabo.
      Voluptate iste dolorum dolor esse. Reiciendis, enim obcaecati facilis eius
      ad modi numquam magni incidunt laboriosam totam. Deleniti quibusdam
      provident, hic consequuntur expedita fugit minus? Laudantium aperiam
      nesciunt id est voluptas culpa maiores et velit.
    </p>
  </div>
);
